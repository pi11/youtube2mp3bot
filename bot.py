#!/usr/bin/env python
# -*- coding: utf-8 -*-

import socket
import os
from urllib.parse import urlparse
from datetime import datetime
import config
import traceback

from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
import logging

from download import download_url, set_tags
from models import Downloads
from helpers import restricted

# Enable logging
logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)

logger = logging.getLogger(__name__)


def help(update, context):
    """Send a message when the command /help is issued."""
    HELP = """Hi! This bot convert videos from youtube.com to mp3.
Just send me youtube video link."""
    update.message.reply_text(HELP)


def message(update, context):
    """Get link"""
    ERROR = "I can't parse this link. Please send me youtube video url."
    o = urlparse(update.message.text)
    correct_url = False
    if len(o.netloc.split(".")) >= 2:
        if ".".join(o.netloc.split(".")[-2:]) == "youtu.be":
            correct_url = True
        if (o.netloc.split(".")[-2] == "youtube") and (o.path == "/watch"):
            correct_url = True
    if not correct_url:
        result = ERROR
        url = False
    else:
        url = update.message.text
        # try:
        #    old = Downloads.select().where(url=url)[0]
        # except IndexError:
        #    pass
        # else:
        #    // send tg file id

        wait_q = (
            Downloads.select()
            .where(Downloads.is_downloaded == False, Downloads.is_failed == False)
            .count()
        )
        user_videos = (
            Downloads.select()
            .where(
                Downloads.is_downloaded == False,
                Downloads.is_failed == False,
                Downloads.chat_id == update.message.chat_id,
            )
            .count()
        )
        if user_videos > 2:
            result = (
                "Sorry, you have already %s videos in queue, please wait till I download them"
                % user_videos
            )
        else:
            if wait_q == 0:
                result = (
                    "I'm downloading video, plase wait. I will send mp3 file shortly."
                )
            else:
                result = (
                    "Url added, I have %s videos in download queue. I will send mp3 file to you as soon as possible"
                    % wait_q
                )
            d = Downloads(chat_id=update.message.chat_id, url=url)
            d.save()
    update.message.reply_text(result)


def error(update, context):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, context.error)


def process_download(context):

    # first send downloaded videos:
    job = context.job
    bot = context.bot
    for d in (
        Downloads.select()
        .where(
            Downloads.is_downloaded == True,
            Downloads.is_sent == False,
            Downloads.send_retries < 4,
        )
        .order_by(Downloads.send_retries)
        .limit(1)
    ):
        print("Sending downloaded file - %s" % d.get_url())
        try:
            message = bot.send_audio(
                timeout=60 * 10,
                chat_id=d.chat_id,
                audio=open(d.filename, "rb"),
                caption="%s: %s \n %s" % (d.uploader, d.title, d.url),
                performer=d.uploader,
                title=d.title,
            )
        except socket.timeout:
            print("Timeout while sending audio")
        except:
            print("Error while sending file...")
            print(traceback.format_exc())
            d.send_retries += 1
            d.save()
            if d.send_retries > 3:
                bot.send_message(
                    chat_id=d.chat_id,
                    text="Sorry, file is too big for sending through telegram API.  Here is the link: %s"
                    % d.get_url(),
                )
        else:
            d.tg_file_id = message.audio.file_id
            d.is_sent = True
            d.save()

    if (
        Downloads.select()
        .where(Downloads.is_started == True, Downloads.is_downloaded == False)
        .count()
        > 0
    ):
        print("Already downloading...")
        print("Limit to only 1 simultaneously download process")
        return

    for d in (
        Downloads.select()
        .where(Downloads.is_downloaded == False, Downloads.is_failed == False)
        .order_by(Downloads.added.desc())
        .limit(1)
    ):
        if d.retries > 2:
            try:
                bot.send_message(
                    chat_id=d.chat_id,
                    text="Sorry, I was not able to download this video:",
                )
                bot.send_message(chat_id=d.chat_id, text=d.url)
            except:
                print(traceback.format_exc())

            d.is_failed = True
            d.is_started = False
            d.save()
        d.retries += 1
        d.is_started = True
        d.save()
        f_name = "tmp/%s.mp3" % d.id
        print("Starting download: %s" % d.url)
        try:
            info = download_url(d.url, "tmp/%s" % d.id)
            set_tags(f_name, info)
        except:
            print(traceback.format_exc())
            d.is_started = False
            d.save()
        else:
            d.uploader = info["uploader"]
            d.title = info["title"]
            d.is_downloaded = True
            d.filename = f_name
            d.save()
            statinfo = os.stat(f_name)
            # print (info)


@restricted
def retry_all(update, context):
    for f in Downloads.select().where(Downloads.is_failed == True):
        f.is_failed = False
        f.save()


# @restricted
def clear_started(update, context):
    for f in Downloads.select().where(
        Downloads.is_started == True, Downloads.is_downloaded == False
    ):
        f.is_started = False
        f.save()


# @restricted
def stat(update, context):
    total = Downloads.select().count()
    downloaded = Downloads.select().where(Downloads.is_downloaded == True).count()
    failed = Downloads.select().where(Downloads.is_failed == True).count()
    update.message.reply_text(
        "Total: {}\nDownloaded: {}\nFailed: {}".format(total, downloaded, failed)
    )


def remove_old_files(update, context):
    interval = datetime.now() - timedelta(days=3)
    for d in Downloads.select().where(Downloads.added < interval):
        os.remove(d.filename)
        print("Removed: %s" % d.filename)
        d.is_file_removed = True
        d.save()


def main():
    """Start the bot."""
    # Create the EventHandler and pass it your bot's token.
    updater = Updater(config.TOKEN, use_context=True)
    jq = updater.job_queue
    d_job = jq.run_repeating(process_download, interval=5, first=0)
    # d_job = jq.run_daily(remove_old_files)

    dp = updater.dispatcher

    dp.add_handler(CommandHandler("start", help))
    dp.add_handler(CommandHandler("help", help))
    dp.add_handler(CommandHandler("stat", stat))
    dp.add_handler(CommandHandler("retry_all", retry_all))
    dp.add_handler(CommandHandler("clear_started", clear_started))

    dp.add_handler(MessageHandler(Filters.text, message))

    dp.add_error_handler(error)

    updater.start_polling()

    updater.idle()


if __name__ == "__main__":
    main()
