import yt_dlp as youtube_dl
from mutagen.easyid3 import EasyID3

def set_tags(filename, info):
    mp3 = EasyID3(filename)
    mp3['artist'] = info['uploader']
    mp3['title'] = info['title']
    mp3['website'] = "https://www.youtube.com/watch?v=%s" % info['id']
    #mp3['comment'] = info['description']
    mp3['encodedby'] = 'tg://@youtube2mp3_advbot'
    mp3.save()
    
def download_url(url, filename):
    tmpl = "%s" % filename + ".%(ext)s"
    ydl_opts = {
        'format': 'bestaudio/best',
        'outtmpl': tmpl,
        'postprocessors': [{
            'key': 'FFmpegExtractAudio',
            'preferredcodec': 'mp3',
            #'preferredquality': '192',
        }],
    }
    if "&list=" in url:
        url = url[0:url.find('&list=')] # do not download playlists
        
    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        info = ydl.extract_info(url, download=True)
    return info
        
if __name__ == "__main__":
    info = download_url('https://www.youtube.com/watch?v=jBSReHa2-bI', "test")
    set_tags('result.mp3', info)
