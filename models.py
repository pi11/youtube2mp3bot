from datetime import datetime
from peewee import *
from config import SITE_URL

db = SqliteDatabase('db/downloads.db')

class Downloads(Model):
    chat_id = IntegerField()
    url = CharField()
    filename = CharField(null=True)
    added = DateTimeField(default=datetime.now)
    is_downloaded = BooleanField(default=False)
    is_failed = BooleanField(default=False)
    is_started = BooleanField(default=False)
    is_sent = BooleanField(default=False)
    retries = IntegerField(default=0)
    send_retries = IntegerField(default=0)
    uploader = CharField(null=True)
    title = CharField(null=True)

    #tg_file_id = models.IntegerField(default=0)
    #is_file_removed = BooleanField(default=False)
    
    def get_url(self):
        return "%s%s" % (SITE_URL, self.filename)
    
    class Meta:
        database = db

if __name__ == "__main__":
    db.connect()
    db.create_tables([Downloads])
    
